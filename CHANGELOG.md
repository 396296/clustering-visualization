### 15.0.0 Update to Angular 15
* a66aef7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   a781a63 -- Merge branch '5-add-local-mock-data-for-static-deployed-demonstration' into 'master'
|\  
| * e8b6977 -- Update to Angular 15
* | e835632 -- Merge branch '5-add-local-mock-data-for-static-deployed-demonstration' into 'master'
|\| 
| * a358225 -- Update lint rules and mock service
| * 0f78cbf -- Prettify the code
| * dcbae84 -- Fix chart display delay
| * 110248e -- Add new mocking service
|/  
* 4986b5a -- Merge branch '4-fix-axes-tooltip' into 'master'
* e34126b -- Resolve "Fix axes tooltip"
### 14.0.2 Propagate insufficient data info for dashboard processing
* 864fbd0 -- [CI/CD] Update packages.json version based on GitLab tag.
* 443a0c3 -- Merge branch '3-indicate-insufficient-data' into 'master'
* 84dc3d3 -- Add control output for dashboard chart display
* 5cd0551 -- Add check for missing data
* 6b652ed -- Add check for NaN and 0
* cd88e95 -- Initial methods for check
### 14.0.1 Update API routes
* e586efc -- [CI/CD] Update packages.json version based on GitLab tag.
* 2e4113e -- Merge branch '2-fix-api-calls' into 'master'
* d018162 -- Update API routes
* 2f30118 -- Fix API calls and json-server routes
### 14.0.0 Initial clustering library version
